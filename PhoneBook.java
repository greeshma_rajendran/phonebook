package phone.book.co.in;

import java.util.HashMap;
import java.util.Scanner;

public class PhoneBook
{
	 private static boolean CheckNumber(String phno)
	 {
		 return phno!=null && phno.length()==8;
	}	
	public static void main(String[] args)
		{
		HashMap<String,String> PhBook = new HashMap<>();
			 Scanner sc = new Scanner(System.in);
			 int limit;
			 boolean validate=false;
			 System.out.println("Enter the limit");
			 limit = sc.nextInt();
			
			 sc.nextLine();
			
			 for (int i=0;i<limit;i++)
			{          
						String name = sc.nextLine().toLowerCase();
						String phno = sc.nextLine();
						validate=CheckNumber(phno);
						 if(!validate)
						  {
							 System.out.println("Please enter 8 digits number");
							 phno = sc.nextLine();
						  }
						 
						PhBook.put(name,phno);
			}
	        System.out.println("Enter the name that you want to search");
			while (sc.hasNext())
			{
					String NameIn = sc.nextLine();
					if (PhBook.containsKey(NameIn.toLowerCase())) 
					{
						System.out.println(NameIn + "=" + PhBook.get(NameIn));
				    } 
					else 
					{
						System.out.println("Not found");
				    }

			}
         
        
		}
	
}